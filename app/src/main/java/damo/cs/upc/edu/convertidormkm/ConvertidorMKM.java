package damo.cs.upc.edu.convertidormkm;

/**
 * Interfície patatera; lísteners per codi
 */

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class ConvertidorMKM extends Activity {
    public static final double FACTOR_DE_CONVERSIO = 1.609344;
    private EditText km;
    private EditText milles;
    private TextWatcher watcherKm;
    private TextWatcher watcherMilles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidor_mkm);
        inicialitza();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.convertidor_mkm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void inicialitza() {
        km =  findViewById(R.id.km);
        milles =  findViewById(R.id.milles);

        watcherKm = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String res = conversio(s.toString(), 1/FACTOR_DE_CONVERSIO);
                milles.setText(res);
            }
        };

        watcherMilles = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
             }

            @Override
            public void afterTextChanged(Editable s) {
                String res = conversio(s.toString(), FACTOR_DE_CONVERSIO);
                km.setText(res);
            }
        };


        km.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) edicioKm(v);
            }
        });

        milles.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) edicioMilles(v);

            }
        });

        /*

        km.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edicioKm(v);
            }
        });


        milles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edicioMilles(v);
            }
        });

        */
    }

    private String conversio(String valor, double factor) {
        Double valorNum;

        try {
            valorNum = Double.valueOf(valor);  // Convertim l'string a un valor
        } catch (NumberFormatException e) {
            return "";
        }
        Double res = valorNum*factor; // Fem la conversió
        return String.valueOf(res); // Reconvertim a string

    }


    private void edicioKm(View v){
        // Suprimim els observadors
        milles.removeTextChangedListener(watcherMilles);
        km.removeTextChangedListener(watcherKm);

        // Esborrem els valors
        milles.setText("");
        km.setText("");

        // Recuperem l'observador
        km.addTextChangedListener(watcherKm);
    }

    private void edicioMilles(View v) {
        // Suprimim els observadors
        milles.removeTextChangedListener(watcherMilles);
        km.removeTextChangedListener(watcherKm);

        // Esborrem els valors
        milles.setText("");
        km.setText("");

        // Recuperem l'observador
        milles.addTextChangedListener(watcherMilles);
    }


}
